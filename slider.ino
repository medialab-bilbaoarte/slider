#include <EEPROM.h>
#include <AccelStepper.h>
#include <SoftwareSerial.h>

//Aquí conectamos los pins RXD (3.3V) Y TDX del módulo Bluetooth.
SoftwareSerial BT(10,11); //10 RX (TDX), 11 TX (RXD).

unsigned long start, elapsed;

bool mueve = false;
//bool para = false;
//int sentido = 1;
int velocidad = 1000;

const int bufferSize = 20; 
char bbuffer[ bufferSize ];
char comando[ 10 ];
char datos[ 15 ];
int byteCount;

// Run a A4998
AccelStepper stepper( 1,5,4 );//initialise accelstepper for a two wire board, pin 5 step, pin 4 dir

const long LARGO = 155800;
const char RELE = 2;

long destino = 0;

void setup() {
  
  BT.begin(9600); 
  
  pinMode( 6, OUTPUT ); // Enable
  digitalWrite( 6,LOW ); // Set Enable low

  pinMode( RELE, OUTPUT );
  
  stepper.setMaxSpeed( 20000 );
  stepper.setAcceleration( 100 );
}

void loop() {

  if ( BT.available() ) {
    parseBTStream();
  }

  if ( byteCount  > 0 ) {
    //BT.print("comando     : ");
    //BT.println(comando);
    //BT.print("datos       : ");
    //BT.println(datos);
    byteCount = -1;
  } 

  // *** Configura la posicion 0
  if ( comandoEs( "home" )) {
    
    setHome();
    BT.println("Home set");
    borrarComandoYDatos();
    
  }

  
  // *** Apagado automático del motor cuando haya llegado a su destino
  if ( abs( stepper.distanceToGo() ) > 0 ) {
    //BT.println(stepper.distanceToGo());
    digitalWrite( RELE, HIGH );
    
  } else {
    
    digitalWrite( RELE, LOW );

    if ( mueve ) {
      elapsed = millis() - start;
      BT.print( "Duracion:  " );
      BT.println( elapsed );
      //BT.print( "Posicion final:" );
      //BT.println( stepper.currentPosition() );
    }
    
    mueve = false;
    
  }

  // *** Movimiento a posición
  if ( comandoEs( "move" )) {

    if ( !mueve ) {
      //BT.print("Posicion inicial:");
      //BT.println(stepper.currentPosition());
    }
    
    mueve = true;
    
    // El dato recibido es la posición destino, que convertimos a int
    sscanf( datos, "%li", &destino );
    
    BT.print( "Moviendo a posicion: " );
    BT.println( destino );
    
    borrarComandoYDatos();

    start = millis();
    
  }

  // *** Movimiento hasta final de carrera NO FUNCIONA
  if ( comandoEs( "slide" )) {
    
    mueve = true;
    
    destino = LARGO;
    
    BT.print( "Moviendo hasta el final" );
    
    borrarComandoYDatos();

    start = millis();
    
  }

  // *** Para
  if ( comandoEs( "stop" )) {
    
    //para = true;
    //mueve = false;
    destino = -stepper.currentPosition();
    stepper.moveTo( destino );
    BT.println( "Parando" );

    borrarComandoYDatos();
    
  }

  if ( comandoEs( "speed" )) {

    sscanf( datos, "%i", &velocidad );
    BT.print( "Velocidad:   " );
    BT.println( velocidad );

    borrarComandoYDatos();
    
  }

  if ( mueve ) {
    
    slideToPositionAtSpeed( destino, velocidad );
    
  } 
}

void parseBTStream() {

//  One command per line.  Eventually, datos may have multiple 
//   fields separated by ":"
//  comando Format: "up to 5 Letter comando, up to 10 letter datos<\n>"
//                  No checking.
//
//  count will  be below Zero on a timeout.
//  read up to X chars or until EOT - in this case "\n" 

 byteCount = -1;
 byteCount =  BT.readBytesUntil( '\n', bbuffer, bufferSize);  
  
 if (byteCount  > 0) {
  
    strcpy( comando, strtok( bbuffer, ":" ));
               
    strcpy( datos, strtok( NULL, "," ));             
 }
 
 memset( bbuffer, 0, sizeof( bbuffer ));   // Clear contents of bbuffer
 
 BT.flush();
 
}

void borrarComandoYDatos() {
  memset( comando, 0, sizeof( comando ));
  memset( datos, 0, sizeof( datos ));
}

 bool comandoEs( char c[] ) {
  
  return strcmp( comando, c ) == 0;
  
}

void setHome() {

  stepper.setCurrentPosition( 0 );
  
}

void slideToPositionAtSpeed( long  position, int speed ) {

  if ( abs( position ) >= 0 && abs( position ) <= LARGO ) {
    
    stepper.runSpeedToPosition();
    stepper.moveTo( -position );
    stepper.setSpeed( speed );
    
  } else {
    
    BT.println( "Perro blando" );
    BT.println( position );
    
  }
  
}
/*
void stopAccel() {

  stepper.stop();
  stepper.runSpeedToPosition(); 

}*/

